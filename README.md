# Instructions to run.
-'Records Validator Microservice' is based on Spring Boot.
-So, just open console and type: `./mvnw spring-boot:run` (if you have no Maven)

# Design approach used
- **Time complexity** is O(n). Service validating each record one by one.
- Reference duplication is checked with O(1) using HashSet cache, so it's not slow down performance.
- **Space complexity** O(n). I'm not streaming, and loading whole xml. Regarding csv, also loading everything
at once, but it can be changed, by loading line by line.
- **Restrictions** For parsing CSV, used OpenCSV library, which has restriction, that file should be < 2GB.
It's easy to fix, by changing OpenCSV to Apache Common CSV.
- Structure of xml should be correct, all of fields should be presented. At the same time, values of those fields
 not required to be valid. Even types can be not correct.

You will receive this error 400 (if xml is not valid at all)

```json
{
    "timestamp": "2018-11-08T09:14:29.075+0000",
    "status": 400,
    "error": "Bad Request",
    "message": "Bad request",
    "path": "/validation/xml"
}
```

 - In CSV, order is matter. System not checks it by column name. This point can be improved.
 - **Encoding** System using ISO_8859_1 to support german/dutch letters.

# How to test
- **Using Integration Test** Run ValidationControllerTest
- testXML() testing xml, testCSV() testing csv in you example.
- Results for XML:

```json
[
    {
        "reference": "156360",
        "description": "Candy for Jan King",
        "errorMessages": [
            "End balance was not calculated correctly"
        ]
    },
    {
        "reference": "115256",
        "description": "Candy from Rik Theuß",
        "errorMessages": [
            "End balance was not calculated correctly"
        ]
    }
]
```

- Results for CSV (about duplication, here we have 3 duplicated items, but 1 of them is allowed, 2 are duplicates of first one):

```json
[
    {
        "reference": "112806",
        "description": "Clothes from Peter de Vries",
        "errorMessages": [
            "Reference already exists"
        ]
    },
    {
        "reference": "112806",
        "description": "Clothes for Daniël Theuß",
        "errorMessages": [
            "Reference already exists"
        ]
    }
]
```

- **Using POSTMAN** Run Application, and open POSTMAN.
- To test XML:
- Method: POST
- URL: http://localhost:8080/validation/xml
- Header: Content-Type: application/xml
- Body: copy-paste your xml

- To test CSV:
- Method: POST
- URL: http://localhost:8080/validation/csv
- Body: upload your csv file

- If you received [], that means, all data is valid

# Possible errors:
- File is not valid
- File is not in csv format
- Reference representing not valid number
- Reference already exists
- Start balance representing not valid number
- Mutation representing not valid number
- End balance representing not valid number
- End balance was not calculated correctly