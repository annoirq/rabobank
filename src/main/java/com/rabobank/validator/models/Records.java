package com.rabobank.validator.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class Records {

    private List<RecordXML> records;

    @XmlElement(name="record")
    public List<RecordXML> getRecords() {
        return this.records;
    }

    public void setRecords(List<RecordXML> records) {
        this.records = records;
    }
}
