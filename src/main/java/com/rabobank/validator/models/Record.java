package com.rabobank.validator.models;

import com.rabobank.validator.validators.BigDecimalNumber;
import com.rabobank.validator.validators.EndBalance;
import com.rabobank.validator.validators.LongNumber;

@EndBalance(message = "End balance was not calculated correctly")
public class Record {
    @LongNumber(message = "Reference representing not valid number")
    private String reference;

    private String accountNumber;
    private String description;

    @BigDecimalNumber(message = "Start balance representing not valid number")
    private String startBalance;

    @BigDecimalNumber(message = "Mutation representing not valid number")
    private String mutation;

    @BigDecimalNumber(message = "End balance representing not valid number")
    private String endBalance;

    public Record() {}

    public Record (Builder builder) {
        this.reference = builder.reference;
        this.accountNumber = builder.accountNumber;
        this.description = builder.description;
        this.startBalance = builder.startBalance;
        this.mutation = builder.mutation;
        this.endBalance = builder.endBalance;
    }

    public String getReference() {
        return reference;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getDescription() {
        return description;
    }

    public String getStartBalance() {
        return startBalance;
    }

    public String getMutation() {
        return mutation;
    }

    public String getEndBalance() {
        return endBalance;
    }

    public static class Builder {
        private String reference;
        private String accountNumber;
        private String description;
        private String startBalance;
        private String mutation;
        private String endBalance;

        public Builder withReference(String reference) {
            this.reference = reference;
            return this;
        }

        public Builder withAccountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withStartBalance(String startBalance) {
            this.startBalance = startBalance;
            return this;
        }

        public Builder withMutation(String mutation) {
            this.mutation = mutation;
            return this;
        }

        public Builder withEndBalance(String endBalance) {
            this.endBalance = endBalance;
            return this;
        }

        public Record build() {
            return new Record(this);
        }
    }
}
