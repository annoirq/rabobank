package com.rabobank.validator.exceptions;


public class RabobankException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private final String message;

    public RabobankException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
