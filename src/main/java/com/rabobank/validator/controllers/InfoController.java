package com.rabobank.validator.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("info")
public class InfoController {

    @GetMapping("version")
    @ResponseBody
    public String getVersion() {
        return "1.0";
    }
}
