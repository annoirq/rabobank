package com.rabobank.validator.controllers;

import com.rabobank.validator.models.RecordError;
import com.rabobank.validator.models.Records;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static com.rabobank.validator.util.CSVRecordsMapperUtil.mapToRecords;
import static com.rabobank.validator.util.RecordValidationUtil.validate;


@RestController
@RequestMapping("validation")
public class ValidationController {

    @PostMapping(value = "xml", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RecordError> validateXML(@RequestBody Records records) {
        return validate(mapToRecords(records.getRecords()));
    }

    @PostMapping(value = "csv", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RecordError> validateCSV(@RequestParam("file") MultipartFile file) {
        return validate(mapToRecords(file));
    }
}
