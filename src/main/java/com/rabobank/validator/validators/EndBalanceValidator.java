package com.rabobank.validator.validators;

import com.rabobank.validator.models.Record;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

public class EndBalanceValidator implements ConstraintValidator<EndBalance, Record> {

    @Override
    public void initialize(EndBalance constraint) {
    }

    @Override
    public boolean isValid(Record record, ConstraintValidatorContext context) {
        try {
            BigDecimal shouldBeEndBalance = new BigDecimal(record.getStartBalance())
                    .add(new BigDecimal(record.getMutation()));

            return shouldBeEndBalance.compareTo(new BigDecimal(record.getEndBalance())) == 0;
        } catch (NumberFormatException e) {
            return true;
        }
    }
}
