package com.rabobank.validator.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LongNumberValidator implements ConstraintValidator<LongNumber, String> {

    @Override
    public void initialize(LongNumber constraint) {
    }

    @Override
    public boolean isValid(String str, ConstraintValidatorContext context) {
        try {
            Long.parseLong(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
