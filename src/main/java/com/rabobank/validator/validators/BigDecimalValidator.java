package com.rabobank.validator.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

public class BigDecimalValidator implements ConstraintValidator<BigDecimalNumber, String> {

    @Override
    public void initialize(BigDecimalNumber constraint) {
    }

    @Override
    public boolean isValid(String str, ConstraintValidatorContext context) {
        try {
            new BigDecimal(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}