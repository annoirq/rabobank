package com.rabobank.validator.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EndBalanceValidator.class)
public @interface EndBalance {
    String message() default "{com.rabobank.validator.validators.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
