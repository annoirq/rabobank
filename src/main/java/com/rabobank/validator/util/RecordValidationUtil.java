package com.rabobank.validator.util;


import com.rabobank.validator.models.Record;
import com.rabobank.validator.models.RecordError;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class RecordValidationUtil {
    private final static String REFERENCE_ALREADY_EXISTS = "Reference already exists";
    private final static Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

    private RecordValidationUtil() {

    }

    public static List<RecordError> validate(List<Record> records) {
        Set<Long> referencesCache = new HashSet<>();
        return records.stream().map(record -> {
            List<String> errorMessage = new ArrayList<>();
            errorMessage.addAll(validateRecord(record));

            if (isValidLongNumber(record.getReference()) && !referencesCache.add(Long.parseLong(record.getReference()))) {
                errorMessage.add(REFERENCE_ALREADY_EXISTS);
            }

            return getRecordError(record, errorMessage);
        })
        .filter(recordErrors -> !recordErrors.getErrorMessages().isEmpty())
        .collect(Collectors.toList());
    }

    private static Set<String> validateRecord(Record record) {
        return VALIDATOR.validate(record).stream().map(ConstraintViolation::getMessage).collect(Collectors.toSet());
    }

    private static boolean isValidLongNumber(String longNumber) {
        try {
            Long.parseLong(longNumber);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static RecordError getRecordError(Record record, List<String> errorMessage) {
        RecordError recordError = new RecordError();
        recordError.setReference(record.getReference());
        recordError.setDescription(record.getDescription());
        recordError.setErrorMessages(errorMessage);

        return recordError;
    }
}
