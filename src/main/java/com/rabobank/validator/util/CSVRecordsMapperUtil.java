package com.rabobank.validator.util;


import com.opencsv.CSVReader;
import com.rabobank.validator.exceptions.RabobankException;
import com.rabobank.validator.models.Record;
import com.rabobank.validator.models.RecordXML;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

public final class CSVRecordsMapperUtil {
    private final static String FILE_IS_NOT_VALID = "File is not valid";
    private final static String FILE_IS_NOT_IN_CSV_FORMAT = "File is not in csv format";

    private CSVRecordsMapperUtil() {

    }

    public static List<Record> mapToRecords(MultipartFile file) {
        if (!file.getContentType().equals("text/csv") && !file.getOriginalFilename().endsWith("csv")) {
            throw new RabobankException(FILE_IS_NOT_IN_CSV_FORMAT);
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream(), StandardCharsets.ISO_8859_1))) {
            return new CSVReader(reader).readAll().stream().skip(1).map(row -> new Record.Builder()
                    .withReference(row[0])
                    .withAccountNumber(row[1])
                    .withDescription(row[2])
                    .withStartBalance(row[3])
                    .withMutation(row[4])
                    .withEndBalance(row[5])
                    .build()).collect(Collectors.toList());
        } catch (IOException e) {
            throw new RabobankException(FILE_IS_NOT_VALID);
        }
    }

    public static List<Record> mapToRecords(List<RecordXML> recordXMLs) {
        return recordXMLs.stream().map(recordXml -> new Record.Builder()
                .withReference(recordXml.getReference())
                .withAccountNumber(recordXml.getAccountNumber())
                .withDescription(recordXml.getDescription())
                .withStartBalance(recordXml.getStartBalance())
                .withMutation(recordXml.getMutation())
                .withEndBalance(recordXml.getEndBalance())
                .build()).collect(Collectors.toList());
    }
}
