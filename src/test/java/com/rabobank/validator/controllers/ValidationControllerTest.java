package com.rabobank.validator.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ValidationControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testXML() throws Exception {
        MvcResult result = this.mockMvc.perform(
                post("/validation/xml")
                        .contentType(MediaType.APPLICATION_XML)
                        .content(readFile("records.xml"))
                )
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();

        assertEquals(response, readFile("records-xml-result.json"));
    }

    @Test
    public void testCSV() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "records.csv", "text/csv", readFileAndGetBytes("records.csv"));

        MvcResult result = this.mockMvc.perform(
                multipart("/validation/csv")
                        .file(file)
                )
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();

        assertEquals(response, readFile("records-csv-result.json"));
    }

    private String readFile(String fileName) {
        try {
            Path path = Paths.get(getClass().getClassLoader().getResource(fileName).toURI());
            Stream<String> lines = Files.lines(path, StandardCharsets.ISO_8859_1);
            String data = lines.collect(Collectors.joining("\n"));
            lines.close();

            return data;
        } catch (Exception e) {
            return "";
        }
    }

    private byte[] readFileAndGetBytes(String fileName) {
        try {
            Path path = Paths.get(getClass().getClassLoader().getResource(fileName).toURI());
            return Files.readAllBytes(path);
        } catch (Exception e) {
            return new byte[0];
        }
    }
}
